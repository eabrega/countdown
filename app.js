﻿var stopTimer;
var startMoney = 50000;

function testTimer(startTime) {

    var bot = document.getElementById("bot");
    bot.setAttribute("disabled", "");

    var time = startTime;
    var money = startMoney;

    var min = parseInt(time / 60);
    if (min < 1) min = 0;
    time = parseInt(time - min * 60);
    if (min < 10) min = '0' + min;

    var seconds = time;
    if (seconds < 10) seconds = '0' + seconds;

    document.getElementById("time").innerHTML = '<span>' + min + ' : ' + seconds + '</span>';
    document.getElementById("money").innerHTML = '<span>' + money + '</span>';

    startTime--;
    startMoney = startMoney - 210;

    if ((startTime >= 0) && (startMoney >= 0)) {
        stopTimer = setTimeout(function () {
            testTimer(startTime);
        }, 1000);

    } else {
        document.getElementById("time").innerHTML = '<span>Время истекло</span>';
        document.getElementById("money").innerHTML = '<span>Денег нет</span>';
        clearTimeout(stopTimer);
        var bot = document.getElementById("bot");
        bot.removeAttribute("disabled", "disabled");
        bot.removeChild(bot.childNodes[0]);
        var text = document.createTextNode("Начать заново");
        bot.appendChild(text);
    }
}

function stop() {
    clearTimeout(stopTimer);
    var bot = document.getElementById("bot");
    bot.removeAttribute("disabled", "disabled");
}