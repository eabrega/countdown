const StartMoney = 100000;
const NotFireMoney = 10000;
const TimeInterval = 1000;
const MoneyStepDown = 210;

class CountDown {
    get fullsec() {
        return this.min * 60 + this.sec;
    }
    set fullsec(value) {
        this.min = parseInt(value / 60);
        this.sec = value - this.min * 60;
    }
    constructor(min, sec) {
        this.isRun = 'false';
        this.min = min;
        this.sec = sec;
        this.delay = TimeInterval;
        this.money = {
            value: StartMoney,
            update: true
        };
    }
    print() {
        let s = this.sec < 10 ? "0" + String(this.sec) : String(this.sec);
        let m = this.min < 10 ? "0" + String(this.min) : String(this.min);
        return m + " : " + s;
    }
    tick() {
        if (this.fullsec > 0) {
            this.fullsec--;
        } else {
            this.isRun = false;
            clearInterval(this.timer);
        }
    }
    control(control) {
        this.isRun = control;
    }
    loop(coolback) {
        this.timer = setInterval(() => {
            if (this.isRun == 'true') {
                this.tick();
                if (coolback != undefined) coolback();
            }
        }, this.delay);
    }
}

// стоп-слово, ну ты в курсе.
var stopWord = NotFireMoney;

// установка минуты - секунды
var Clock = new CountDown(10, 0);

Clock.loop(() => {
    if (Clock.money.update) Clock.money.value = (Clock.money.value - MoneyStepDown > 0) ? Clock.money.value - MoneyStepDown : 0;

    if (Clock.money.value < stopWord) {
        Clock.money.value = NotFireMoney;
        stopWord = 0;
        Clock.control(false);
    }
    display_update(Clock);
});

var BigButtonValue = document.getElementById("big").getAttribute("value");
var SmallButtonValue = document.getElementById("small").getAttribute("value");

document.addEventListener("keyup", moveRect);
document.getElementById("time").innerText = Clock.print();
document.querySelectorAll("button.money").forEach(element => {
    element.addEventListener("click", money_control)
});
document.querySelectorAll("button.clock").forEach(element => {
    element.addEventListener("click", clock_control)
});
document.getElementById("money").innerText = StartMoney;

function money_control(event) {
    let param = typeof (event) == 'object' ? event.target.value : event;
    let newMoney = Clock.money.value - param;

    Clock.money.value = newMoney > stopWord ? newMoney : stopWord;

    display_update(Clock);
    clock_control("true");
}

function clock_control(event) {
    let param = typeof (event) == 'object' ? event.target.value : event;
    Clock.control(param);
    display_update(Clock);
}

function display_update(clock) {
    document.getElementById("money").innerText = clock.money.value;
    document.getElementById("time").innerText = clock.print();

    if (clock.fullsec === 0 || clock.money.value === 0) {
        document.getElementById("money").innerText = "";
        document.getElementById("time").innerText = "ПЕЗДЕЦ!";
        clock.control(false);
    }
}

function moveRect(e) {
    switch (e.keyCode) {
        case 49: // если нажата клавиша "1" (старт)
            clock_control("true");
            break;
        case 50: // если нажата клавиша "2" (стоп)
            clock_control("false");
            break;
        case 51: // если нажата клавиша "3" (-10000)
            money_control(BigButtonValue);
            break;
        case 53: // если нажата клавиша "5" (- 5000)
            money_control(SmallButtonValue);
            break;
        case 54: // если нажата клавиша "6" (Супер игра)
            clock_control("false");

            Clock.sec = 0;
            Clock.min = 2;
            Clock.money = {
                value: Clock.money.value * 2,
                update: true
            }

            display_update(Clock);
            break;
        case 57: // если нажата клавиша "9" (Херобора)
            Clock.money = {
                value: Clock.money.value - SmallButtonValue,
                update: false
            };

            display_update(Clock);
            clock_control("true")
            break;
    }
}